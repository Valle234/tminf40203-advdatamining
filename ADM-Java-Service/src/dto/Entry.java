package dto;

public class Entry {
	private String journal = "";
	private String id = "";
	private String authors = "";
	private String title = "";
	private String institutions = "";
	private String content = "";
	private String pmid = "";

	public final String getJournal() {
		return journal;
	}

	public final void addJournal(String journal) {
		this.journal += journal;
	}

	public final String getId() {
		return id;
	}

	public final void addId(String id) {
		this.id += id;
	}

	public final String getAuthors() {
		return authors;
	}

	public final void addAuthor(String author) {
		this.authors += author;
	}

	public final String getTitle() {
		return title;
	}

	public final void addTitle(String title) {
		this.title += title;
	}

	public final String getInstitutions() {
		return institutions;
	}

	public final void addInstitutions(String institutions) {
		this.institutions += institutions;
	}

	public final String getContent() {
		return content;
	}

	public final void addContent(String content) {
		this.content += content;
	}

	public final String getPmid() {
		return pmid;
	}

	public final void addPmid(String pmid) {
		this.pmid += pmid;
	}
	
	public void setJournal(String journal) {
		this.journal = journal;
	}

	public void setId(String id) {
		this.id = id;
	}

	public void setAuthors(String authors) {
		this.authors = authors;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public void setInstitutions(String institutions) {
		this.institutions = institutions;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public void setPmid(String pmid) {
		this.pmid = pmid;
	}

	@Override
	public String toString() {
		return "journal=" + journal + ",\n id=" + id + ",\n authors=" + authors + ",\n title=" + title
				+ ",\n institutions=" + institutions + ",\n content=" + content + "\n, pmid=" + pmid;
	}


}
