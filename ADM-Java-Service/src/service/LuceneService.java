package service;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

import javax.ws.rs.Consumes;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.apache.lucene.queryparser.classic.ParseException;

import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import engine.ImportDocument;
import engine.LuceneManager;
import engine.QueryInput;
import json.LuceneServiceException;
import json.Result;

@Path("/query")
public class LuceneService {
	
	/**
	 * Path & File parameters
	 */
	private static final String INPUT_FILE_NAME = "input.txt";
	private static final String INDEX_FOLDER_NAME = "luceneIndex";
	private static final String INPUT_FOLDER_NAME = "input";
	private static final String PATH_OF_SERVICE = LuceneService.class.getProtectionDomain().getCodeSource().getLocation().getPath();
	private static final String PATH_OF_INDEX = getIndexOrInputPath(INDEX_FOLDER_NAME);
	private static final String PATH_OF_INPUT = getIndexOrInputPath(INPUT_FOLDER_NAME);
	
	/**
	 * Default-Values of parameters.
	 */
	private static final int MAX_RESULTS_PER_PAGE = 1000;
	private static final String DEFAULT_START_INDEX = "0";
	
	/**
	 * Additional Parameters
	 */
	private static final String LENGTH = "length";
	private static final String START_INDEX = "startIndex";
	
	/**
	 * Core-Parameters
	 */
	public static final String ID = "id";
	public static final String JOURNAL = "journal";
	public static final String AUTHORS = "authors";
	public static final String TITLE = "title";
	public static final String INSTITUTIONS = "institutions";
	public static final String ABSTRACT = "abstract";
	public static final String CONTENT = "content";
	public static final String PMID = "pmid";
	public static final String RANKING = "ranking";
	public static final String ALL = "all";
	public static final String SEARCH = "search";
	
	/**
	 * Charset of Response
	 */
	private static final String CHARSET = "; charset=utf-8";
	
	/**
	 * Execute lucene search, if the index hasn't been created yet, it will be done now.
	 * 
	 * @param resultsPerPage
	 * @param index
	 * @param id
	 * @param journal
	 * @param authors
	 * @param title
	 * @param institutions
	 * @param content
	 * @param pmid
	 * @param all
	 * @param search
	 * @return
	 * @throws JsonProcessingException
	 */
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.TEXT_PLAIN)
	public Response executeLuceneQuery(
			@DefaultValue("" + MAX_RESULTS_PER_PAGE) @QueryParam(LENGTH) String resultsPerPage,
			@DefaultValue(DEFAULT_START_INDEX) @QueryParam(START_INDEX) String index,
			@QueryParam(ID) String id,
			@QueryParam(JOURNAL) String journal,
			@QueryParam(AUTHORS) String authors,
			@QueryParam(TITLE) String title,
			@QueryParam(INSTITUTIONS) String institutions,
			@QueryParam(ABSTRACT) String content,
			@QueryParam(PMID) String pmid,
			@QueryParam(ALL) String all,
			@QueryParam(SEARCH) String search
			) throws JsonProcessingException {
		 // create the mapper
        ObjectMapper mapper = new ObjectMapper();
        // do not show null-value-fields
        mapper.setSerializationInclusion(Include.NON_NULL);
        
		String json;
		try{
			// create luceneIndex if it doesn't exist
			if(!isIndexCreated()){
				createIndex();
			}
			// execute lucene search
			int intResultsPerPage = Integer.parseInt(resultsPerPage);
			//maxAllowedResults = MAX_RESULTS_PER_PAGE
			if(intResultsPerPage > MAX_RESULTS_PER_PAGE){
				intResultsPerPage = MAX_RESULTS_PER_PAGE;
			}
			int intIndex = Integer.parseInt(index);
			Result result;
			QueryInput query = new QueryInput(PATH_OF_INDEX, LuceneManager.ANALYZER);
			// search for custom lucene query-string
			if(search != null){
				search = prepareStringForSearch(search);
				result = query.queryCustomSearch(search, intResultsPerPage, intIndex);
			}
			// search the value of 'all' in all attributes
			else if(all != null){
				result = query.queryMultiAttributes(all.toLowerCase(), intResultsPerPage, intIndex);
			}
			// search for given attribute-value-pairs
			else{
				Map<String, String> params = buildParamsMap(id,authors, title, institutions,
						content, pmid);
				result = query.queryMultiValues(params, intResultsPerPage, intIndex);
			}
	
	        // serialize the object
	        json = mapper.writeValueAsString(result);
		}
		catch(NumberFormatException e){
			json = mapper.writeValueAsString(new LuceneServiceException(Status.BAD_REQUEST.getStatusCode(), Status.INTERNAL_SERVER_ERROR.name(), "NumberFormatException: " + e.getMessage()));
			return Response.status(Status.BAD_REQUEST).entity(json)
					.header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON + CHARSET)
					.build();
		}
		catch(IOException e){
			json = mapper.writeValueAsString(new LuceneServiceException(Status.INTERNAL_SERVER_ERROR.getStatusCode(), Status.INTERNAL_SERVER_ERROR.name(), "IOException: " + e.getMessage()));
			return Response.status(Status.INTERNAL_SERVER_ERROR).entity(json)
					.header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON + CHARSET)
					.build();
		}
		catch(ParseException e){
			json = mapper.writeValueAsString(new LuceneServiceException(Status.INTERNAL_SERVER_ERROR.getStatusCode(), Status.INTERNAL_SERVER_ERROR.name(), "ParseException: " + e.getMessage()));
			return Response.status(Status.INTERNAL_SERVER_ERROR).entity(json)
					.header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON + CHARSET)
					.build();
		}
		
		return Response.ok(json, MediaType.APPLICATION_JSON)
				.header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON + CHARSET)
				.build();
	}

	/**
	 * Prepare searchString for lucene-search
	 * @param search
	 * @return
	 */
	private String prepareStringForSearch(String search) {
		search = search.replace(ABSTRACT, CONTENT);
		return search;
	}

	/**
	 * Build Map of attribute-value pairs.
	 * 
	 * @param id
	 * @param authors
	 * @param title
	 * @param institutions
	 * @param content
	 * @param pmid
	 * @return
	 * @throws UnsupportedEncodingException
	 */
	private Map<String, String> buildParamsMap(String id, String authors, String title,
			String institutions, String content,
			String pmid) throws UnsupportedEncodingException {
		Map<String, String> params = new HashMap<String, String>();
		if(id != null){
			params.put(ID, id.toLowerCase());
		}
		if(authors != null){
			params.put(AUTHORS, authors.toLowerCase());
		}
		if(title != null){
			params.put(TITLE, title.toLowerCase());
		}
		if(institutions != null){
			params.put(INSTITUTIONS, institutions.toLowerCase());
		}
		if(content != null){
			params.put(CONTENT, content.toLowerCase());
		}
		if(pmid != null){
			params.put(PMID, pmid.toLowerCase());
		}

		return params;
	}

	/**
	 * 
	 * @return file-separator of PATH_OF_SERVICE
	 */
	private static String getSeparator() {
		int positionOfSeparator = PATH_OF_SERVICE.lastIndexOf("classes");
		return PATH_OF_SERVICE.substring(positionOfSeparator - 1, positionOfSeparator);
	}

	/**
	 * Creates the path to the specified folder.
	 * @param folder
	 * @return path to the folder
	 */
	private static String getIndexOrInputPath(String folder) {
		final String separator = getSeparator();
		String[] pathSplitted = PATH_OF_SERVICE.split(separator);
		StringBuilder pathToFolder = new StringBuilder();
		if(pathSplitted != null && pathSplitted.length > 3){
			int count = 1;
			for (String part : pathSplitted) {
				if(count < pathSplitted.length - 3){
					pathToFolder.append(part);
					pathToFolder.append(separator);
				}
				count++;
			}
			pathToFolder.append(folder);
			pathToFolder.append(separator);
		}
		return pathToFolder.toString();
	}

	/**
	 * Creates the luceneIndex at PATH_OF_INDEX.
	 * @throws IOException
	 * @throws GeneralEvaluationException
	 */
	private void createIndex() throws IOException {
		LuceneManager luceneManager = LuceneManager.getInstance();
		luceneManager.initIndexer(PATH_OF_INDEX);
		ImportDocument importDocument = ImportDocument.getInstance();
		importDocument.readFile(PATH_OF_INPUT + INPUT_FILE_NAME);
		luceneManager.closeWriter();
	}

	/**
	 * Checks if PATH_OF_INDEX exists
	 * @return true if path exists, else false
	 */
	private boolean isIndexCreated() {
		if(new File(PATH_OF_INDEX).exists()){
			return true;
		}
		return false;
	}
}
