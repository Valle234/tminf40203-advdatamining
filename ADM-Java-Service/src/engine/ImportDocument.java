package engine;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;

import dto.Entry;

/**
 * Class for importing a pubmed file and parsing all entries. Singleton enabled.
 */
public class ImportDocument {

	private CoreState currentCoreState = null;
	private static final ImportDocument instance = new ImportDocument();

	/**
	 * Private constructor for singleton
	 */
	private ImportDocument() {
	}

	/**
	 * Returns instance of self-instantiating class.
	 * 
	 * @return instance
	 */
	public static ImportDocument getInstance() {
		return instance;
	}


	private enum CoreState {
		JOURNAL, TITLE, AUTHOR, INSTITUTION, CONTENT, PMID;

		private static CoreState[] states = values();

		public CoreState next() {
			return states[(this.ordinal() + 1) % states.length];
		}
	}

	/**
	 * Reads given file and parses entries. Entry will be pushed into index.
	 * 
	 * @param path
	 *            - filename to read
	 * @throws IOException
	 * @throws GeneralEvaluationException
	 */
	public void readFile(String path) throws IOException {
		FileInputStream stream = new FileInputStream(path);
		BufferedReader reader = new BufferedReader(new InputStreamReader(stream));
		String line = reader.readLine();
		currentCoreState = CoreState.JOURNAL;
		Entry entry = new Entry();
		boolean lastLineEmpty = false;
		
		while ((line = reader.readLine()) != null) {
			line = line.trim();
			if(line != null && !line.isEmpty()){
				if(line.startsWith("PMID") || line.startsWith("PMCID")){
					currentCoreState = CoreState.PMID;
				}
				else if(line.startsWith("[Article in")){
					// skip line with language
					reader.readLine();
					line = reader.readLine();
					if(line != null && !line.isEmpty()){
						line = line.trim();
					}
				}
				if(CoreState.PMID.equals(currentCoreState)
						&& !(line.startsWith("PMID") || line.startsWith("PMCID"))){
					currentCoreState = CoreState.CONTENT;
				}
				lastLineEmpty = false;
				switch(currentCoreState){
				case JOURNAL:
					entry = new Entry();
					String id = getIdFromLine(line);
					entry.setId(id);
					entry.addJournal(line);
					break;
					
				case TITLE:
					entry.addTitle(line);
					break;
					
				case AUTHOR:
					entry.addAuthor(line);
					break;
					
				case INSTITUTION:
					entry.addInstitutions(line);
					break;
					
				case CONTENT:
					entry.addContent(line);
					break;
					
				case PMID:
					entry.addPmid(line);
					break;
				}
				
			}
			else{
				if(CoreState.PMID.equals(currentCoreState)){
					System.out.println("Entry with id " + entry.getId() + " added to index.");
					LuceneManager.getInstance().writeDocument(entry);
					entry = new Entry();
				}
				if(!lastLineEmpty){
					currentCoreState = currentCoreState.next();
				}

				lastLineEmpty = true;
			}
		}
		if(reader != null){
			reader.close();
		}
	}

	/**
	 * Gets the id from this line (journal).
	 * 
	 * @param line
	 * @return
	 */
	private String getIdFromLine(String line) {
		StringBuilder result = new StringBuilder();
		for (char c : line.toCharArray()) {
			if(Character.isDigit(c)){
				result.append(c);
			}
			else {
				return result.toString();
			}
		}
		return null;
	}
}