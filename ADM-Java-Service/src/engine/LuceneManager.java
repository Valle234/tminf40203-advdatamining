package engine;

import java.io.File;
import java.io.IOException;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.TextField;
import org.apache.lucene.document.Field.Store;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.index.IndexWriterConfig.OpenMode;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;

import dto.Entry;
import service.LuceneService;

/**
 * Contains all methods communication with Lucene framework.
 *
 */
public class LuceneManager {
	private IndexWriter writer;
	private Directory dir;
	public static final Analyzer ANALYZER = new StandardAnalyzer();
	
	private static final LuceneManager instance = new LuceneManager();

	/**
	 * Private constructor for singleton.
	 */
	private LuceneManager() {
	}

	/**
	 * Returns instance for singleton.
	 * 
	 * @return instance
	 */
	public static LuceneManager getInstance() {
		return instance;
	}

	/**
	 * Initializing of Lucene Indexer using given path. In this path, the lucene
	 * index will be created.
	 * 
	 * @param path
	 *            - directory
	 * @throws IOException
	 */
	public void initIndexer(String path) throws IOException {
		dir = FSDirectory.open(new File(path).toPath());
		IndexWriterConfig config = new IndexWriterConfig(ANALYZER);
		config.setOpenMode(OpenMode.CREATE);
		writer = new IndexWriter(dir, config);
	}

	/**
	 * Write an entry directly to Lucene Index (has to be initiated before).
	 * 
	 * @param entry
	 * @throws IOException 
	 */
	public void writeDocument(Entry entry) throws IOException {
		Document doc = new Document();
		doc.add(new TextField(LuceneService.JOURNAL, entry.getJournal(), Store.YES));
		doc.add(new TextField(LuceneService.ID, entry.getId(), Store.YES));
		doc.add(new TextField(LuceneService.AUTHORS, entry.getAuthors(), Store.YES));
		doc.add(new TextField(LuceneService.TITLE, entry.getTitle(), Store.YES));
		doc.add(new TextField(LuceneService.INSTITUTIONS, entry.getInstitutions(), Store.YES));
		doc.add(new TextField(LuceneService.CONTENT, entry.getContent(), Store.YES));
		doc.add(new TextField(LuceneService.PMID, entry.getPmid(), Store.YES));
		writer.addDocument(doc);
	}
	
	public void closeWriter() throws IOException{
		if(writer != null){
			writer.close();
		}
	}
}
