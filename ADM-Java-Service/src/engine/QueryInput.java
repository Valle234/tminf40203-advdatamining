package engine;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.search.BooleanClause;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.queryparser.classic.MultiFieldQueryParser;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TopScoreDocCollector;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;

import json.Result;
import service.LuceneService;

public class QueryInput {
	
	private Directory indexDir;
	private Analyzer analyzer;
	
	private Result result;
	
	private final String[] allFields = {LuceneService.JOURNAL, LuceneService.ID, LuceneService.AUTHORS
			, LuceneService.TITLE, LuceneService.INSTITUTIONS, LuceneService.CONTENT, LuceneService.PMID};
	
	public QueryInput(String indexPath, Analyzer analyzerGet) throws IOException{
		this.indexDir = FSDirectory.open(new File(indexPath).toPath());;
		this.analyzer = analyzerGet;
	}
	
	/**
	 * Execute custom search, if parameter 'search' is filled.
	 * 
	 * @param query
	 * @param hitsPerPage
	 * @param startIndex
	 * @return
	 * @throws ParseException
	 * @throws IOException
	 */
	public Result queryCustomSearch(String query, int hitsPerPage, int startIndex) throws ParseException, IOException{
		if(query != null && !query.isEmpty()){
			QueryParser parser = new MultiFieldQueryParser(allFields, analyzer);
			Query searchQuery = parser.parse(query);
			searchQuery(searchQuery, hitsPerPage, startIndex);
		}
		return result;
	}
	
	/**
	 * Execute search, if parameter 'all' is filled. Operator = OR.
	 * 
	 * @param value
	 * @param hitsPerPage
	 * @param startIndex
	 * @return
	 * @throws IOException
	 * @throws ParseException
	 */
	public Result queryMultiAttributes(String value, int hitsPerPage, int startIndex) throws IOException, ParseException{
		if(value != null && !value.isEmpty()){
			BooleanClause.Occur[] occurs = new BooleanClause.Occur[allFields.length];
			for(int i=0; i<allFields.length; i++){
				occurs[i] = BooleanClause.Occur.SHOULD;
			}
			Query searchQuery = MultiFieldQueryParser.parse(value, allFields, occurs, analyzer);
			searchQuery(searchQuery, hitsPerPage, startIndex);
		}
		return result;
	}
	
	/**
	 * Execute search for all key-value-pairs. Operator = AND.
	 * 
	 * @param params
	 * @param hitsPerPage
	 * @param startIndex
	 * @return
	 * @throws IOException
	 * @throws ParseException
	 */
	public Result queryMultiValues(Map<String, String> params, int hitsPerPage, int startIndex) throws IOException, ParseException{
		if(params != null && !params.isEmpty()){
			Collection<String> keys = new ArrayList<>();
			Collection<String> values = new ArrayList<>();
			Collection<BooleanClause.Occur> occur = new ArrayList<>();
			for(java.util.Map.Entry<String, String> param : params.entrySet()){
				keys.add(param.getKey());
				values.add(param.getValue());
				occur.add(BooleanClause.Occur.MUST);
			}
			Query searchQuery = MultiFieldQueryParser.parse(values.toArray(new String[values.size()]),
					keys.toArray(new String[keys.size()]), occur.toArray(new BooleanClause.Occur[occur.size()]), analyzer);
			searchQuery(searchQuery, hitsPerPage, startIndex);
		}
		return result;
	}
	
	/**
	 * Execute Query and return result.
	 * 
	 * @param queryString
	 * @param hitsPP
	 * @param startIndex
	 * @throws IOException
	 */
	private void searchQuery(Query query, int hitsPerPage, int startIndex) throws IOException{
	        
			result = new Result();
	        IndexReader reader = DirectoryReader.open(indexDir);
	        IndexSearcher searcher = new IndexSearcher(reader);
	        int numHits = hitsPerPage + startIndex;
	        TopScoreDocCollector collector = TopScoreDocCollector.create(numHits);
	        //Search
	        searcher.search(query, collector);
	        ScoreDoc[] hits = collector.topDocs(startIndex, hitsPerPage).scoreDocs;
	        
	        int totalResults = collector.getTotalHits();
	        
	        System.out.println("Search:" + query );
	        System.out.println("Search results " + hits.length);  
	        
	        result.setIndex(startIndex);
	        result.setResultsPerPage(hitsPerPage);
	        result.setTotalResults(totalResults);
	        
	        // Select results
	        for (int i = 0; i < hits.length; ++i) {
	   
	            int docId = hits[i].doc;
	            float actualRank = hits[i].score;
	            
	            Document d = searcher.doc(docId);
	            result.addDoc(createResultElement(d, actualRank));
	        }

	}
	
	/**
	 * Map resultDocument to json.Document.
	 * 
	 * @param resultDocument
	 * @param searchRank
	 * @return
	 */
	private json.Document createResultElement(Document resultDocument, float searchRank){
		
		json.Document doc = new json.Document();
		
		String authors = resultDocument.get(LuceneService.AUTHORS);
		String journal = resultDocument.get(LuceneService.JOURNAL);
		String id = resultDocument.get(LuceneService.ID);
		String title = resultDocument.get(LuceneService.TITLE);
		String institutions = resultDocument.get(LuceneService.INSTITUTIONS);
		String content = resultDocument.get(LuceneService.CONTENT);
		String pmid = resultDocument.get(LuceneService.PMID);
		
		if(authors != null && !authors.isEmpty()){
			String[] authorsArray = authors.split(",");
			if(authorsArray.length > 0){
				Collection<String> authorsList = new ArrayList<String>();
				for (String auth : authorsArray) {
					authorsList.add(auth);
				}
				doc.setAuthors(authorsList);
			}
		}
		if(journal != null && !journal.isEmpty()){
			doc.setJournal(journal);
		}
		if(id != null && !id.isEmpty()){
			doc.setId(id);
		}
		if(title != null && !title.isEmpty()){
			doc.setTitle(title);
		}
		if(institutions != null && !institutions.isEmpty()){
			doc.setInstitutions(institutions);
		}
		if(content != null && !content.isEmpty()){
			doc.setContent(resultDocument.get(LuceneService.CONTENT));
		}
		if(pmid != null && !pmid.isEmpty()){
			doc.setPmid(mapPmid(pmid));
		}
		doc.setRanking(searchRank);
				
		return doc;
	}
	
	/**
	 * Parses the PMID out of the string.
	 * 
	 * @param pmid
	 * @return
	 */
	private String mapPmid(String pmid) {
		final String searchPmid = "PMID:";
		StringBuilder result = new StringBuilder();
		int startPos = pmid.indexOf(searchPmid);
		if(startPos >= 0){
			pmid = pmid.substring(startPos + searchPmid.length());
			for (char c : pmid.toCharArray()) {
				if(Character.isDigit(c)){
					result.append(c);
				}
				else if(result.length() > 0){
					return result.toString();
				}
			}
		}
		
		return null;
	}
}
