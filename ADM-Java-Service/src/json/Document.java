package json;

import java.util.Collection;

import com.fasterxml.jackson.annotation.JsonProperty;

import service.LuceneService;

public class Document {
	
	@JsonProperty(LuceneService.ID)
	private String id;
	
	@JsonProperty(LuceneService.JOURNAL)
	private String journal;
	
	@JsonProperty(LuceneService.AUTHORS)
	private Collection<String> authors;
	
	@JsonProperty(LuceneService.TITLE)
	private String title;
	
	@JsonProperty(LuceneService.INSTITUTIONS)
	private String institutions;
	
	@JsonProperty(LuceneService.ABSTRACT)
	private String content;
	
	@JsonProperty(LuceneService.PMID)
	private String pmid;
	
	@JsonProperty(LuceneService.RANKING)
	private float ranking;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getJournal() {
		return journal;
	}

	public void setJournal(String journal) {
		this.journal = journal;
	}

	public Collection<String> getAuthors() {
		return authors;
	}

	public void setAuthors(Collection<String> authors) {
		this.authors = authors;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getInstitutions() {
		return institutions;
	}

	public void setInstitutions(String institutions) {
		this.institutions = institutions;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getPmid() {
		return pmid;
	}

	public void setPmid(String pmid) {
		this.pmid = pmid;
	}

	public float getRanking() {
		return ranking;
	}

	public void setRanking(float ranking) {
		this.ranking = ranking;
	}

}
