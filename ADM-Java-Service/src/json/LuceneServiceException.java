package json;

public class LuceneServiceException extends Exception{

	/**
	 * 
	 */
	private static final long serialVersionUID = -6672640823452604675L;

	private int code;
	private String status;
	private String message;

	public LuceneServiceException(int code, String status, String message) {
		super();
		this.code = code;
		this.status = status;
		this.message = message;
	}
	
	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
}
