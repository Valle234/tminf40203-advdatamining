package json;

import java.util.ArrayList;
import java.util.Collection;

public class Result {
	
	private int totalResults;
	private int resultsPerPage;
	private int index;
	private Collection<Document> results = new ArrayList<>();

	public Collection<Document> getResults() {
		return results;
	}


	public void setResults(Collection<Document> results) {
		this.results = results;
	}


	public void addDoc(Document document) {
		this.results.add(document);
	}


	public int getTotalResults() {
		return totalResults;
	}


	public void setTotalResults(int totalResults) {
		this.totalResults = totalResults;
	}


	public int getResultsPerPage() {
		return resultsPerPage;
	}


	public void setResultsPerPage(int resultsPerPage) {
		this.resultsPerPage = resultsPerPage;
	}


	public int getIndex() {
		return index;
	}


	public void setIndex(int index) {
		this.index = index;
	}
}
